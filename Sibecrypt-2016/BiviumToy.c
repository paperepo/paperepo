define len  59;
define lenA 31;
define lenB 28;
define stream_len 65;
__in bit reg[len];
__out bit stream[stream_len];

void shift_regs(){
	bit t1 = reg[21]^reg[28]&reg[29]^reg[30]^reg[55];
	bit t2 = reg[53]^reg[56]&reg[57]^reg[58]^reg[22];
	int i;
	for(i = lenA - 1; i > 0; i = i - 1){
		reg[i] = reg[i-1];
	}
	reg[0] = t2;
	for(i = lenA + lenB - 1; i > lenA; i = i - 1){
		reg[i] = reg[i-1];
	}
	reg[lenA] = t1;
}

void main(){
	for(int i = 0; i < stream_len; i = i + 1){
		bit t1 = reg[21] ^ reg[30];
		bit t2 = reg[53] ^ reg[58];
		stream[i] = t1 ^ t2;
		shift_regs();
	}
}