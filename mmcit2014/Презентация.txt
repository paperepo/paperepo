
1) ���-�������: �����������, ����, ������� ����������.
����������������� ���-�������: �����������, ������� ����������, ������������ ������ ������������ �� �������.

���-������� - ��� ���������� �������, ������������� ������� ������ ������������ ����� � ������� ������ ������������� �����.
����� ����� ������� ���������� �����.
����������� ������ ����������� � ���������������� ��� ���������� ������������� ��������, ������ ���������� � ������� ������, 
���������� ���������� ��������������� ��� ������� ������, ������������ ������������, ��� �������� ������� � �������� ������, 
��� ��������� ���.
� ����� ������ ������������ ������������ ����� ��������� ������� � ���-����� ��� - ��� ������ ���� ������ ���������� ��������� ����������.

� ����������������� �������� ����������� �������������� ����������.
��� ����, ����� ���-������� H ��������� ���������������� �������, ��� ������ ������������� 3 �������� �����������:
1. �������������.
2. ��������� � ��������� ������� ����.
3. ��������� � ��������� ������� ����.

��������, ������ ���������� �� �������� ������������:
1. ��������� ������� �� ������ � ��������� ������� � ������� ����.
2. �������, ��������� � ��������� ������� ����, �������� � � ��������� ������� ���� (�������� �������).

������� ��������, ��� �� �������� ��� �������������� ���-������� ����������, ��� ���������� ��������� �������� �������� 
������������� ������� �������, � ������� ������ ���������� �� �������� �����. ��� ���� ������ ��������, ��� �������, 
������ �� ��������.
��� ����������������� ������� ����� ����� ������� �.�. ��������� ������� - �������� ��������� ��������� ������ ������ �������� �������.
�������� ���� �� ������ ������ ������ ���������� ���� �� ��������� ����� ��������� - ��� ���������� �������� ������� 
��������������� ���������� ����������� ���������������� ������� ��� ��������� ������.

2) ������ ������ �������� ����������������� ���-�������, ����� ����������. 
������������ ��� ����� ������ (����� SAT).

1. ����� "���� ��������".
� ������ ����� ����� �������� ���� ��������.
��� �������� ��� ������� f ����� ����� �������� ����� �������� ������� ����.
��� ����� ����������� �������� f ��� �������� ��������� ������ ������� ������ �� ��� ���, ���� �� ����� ������� ��� �����, 
������� ���� � ��� �� ���.
����� "���� ��������" ��������� �������� �������� ��� ���-������� � ������ �������� n ����� � ������� 
�� �������� 2^(n/2) ���������� ���-�������. ������� n-������ ���-������� ��������� ���������������� �������, ���� �������������� 
��������� ���������� �������� ��� ��� ������ � 2^(n/2).

2. �������� ���-������� MD4 � MD5. ����� ��� ������.
������������ ��� ����, ��� MD5 �������� �� ������������ ������ ������-��������.
�������� ����� ������ ����������� ������������������ 16-������� ��������� s_0,...,s_n �� ������� s_(i+1) = f(s_i, M_i).
��������� ��������� s_0 - ���������������� ������.
����� ��� ��������� ��� ������������� ��������� ����������������� ������� ����� ��� ���� M, M' � N, N' ����� ��� 
f(f(s, M), M') = f(f(s, N), N').



3) ����������� ������������ SAT ��� ������������ ������� ����������������� �������:
��� ����� SAT, ��� �� ��������� (��������� ����� ������� �����������), 

����������� ��������� �������������� ���������� ���� ����������� ������ ������������� ������ ������������ ������������.
�������� ������� �������� �������� � ����������� ������� ������ ������� ���������, � SAT-�����������.
��� SAT-������������ ������ � �������� ������ ����������� ������� ������ ������� ���������.
���������� �������� ������ ��������� ��� �������� � ���� SAT-������ �� �������� ����������� ������������ 
����������� SAT-�������� ��� ������ �������.

(����� �������� �� ����� �������� ����������� ��������� ��� ������ �� �������)

4) �������� ����������� ������ � ���� ������� ������� ���������.

��������� �������� �� �������� ���������� � ������� ������� ��������� ���������� ����������������� ������������.
��� ���� ����� ������� ��� �� ���� ����� �������� ������������� ���������, ��������� ��������� ����������� ��������� 
������������ ������ �� ������������� ������ ��������.
� ����� ������ ��� ������������������ ����������� ���������� �������������� � �� ������� �� ���������� ���������� ������������� 
����������� �������� - ���������� Transalg.
�� ������� SAT-������ �� ������ ���������� ��������� ����� ��� �������� ������: ��� ������ ������ ��������� ������� ���������� ��������,
��� ����� ������ �������� ������� ������� ��������.

5) ���������� Transalg: ����� �������� ������.

����� ���������� �����, ������������ �������� ���� ����������.

6) ������ ��������� �� ��-����� ��� MD5 (��������), ������� �������� �����������.

����� �������� ���������...

7) ��������� ������������ ��������� ���-�������������� (MD4, MD5, SHA-1) ��� ������ � 2-� ������.
	Transalg     Mironov & Zhang
       Vars  Clauses Vars   Clauses
MD4    8757  86730   53228  221440
MD5    16637 145765  89748  375176
SHA1   22531 226339  114809 486185


8) ���������� �������������� �������: 
��������� ����� ����� ���������, ������ ��������� ���������� ����, ������ ���������, ������� ��� ������ ��������� ���������.

9) ��������� ��������� � ��������������� ���������.

10) ���������� ������ ���� ��� ������ �������� �� ������� MD4 (�� ���� ��������� ������ ����).
������� � ����� ������. ��� ����� ����������� � ����� ����������.

11) ���������� ������ ��������