void main() {
	bit a[32] = A; 
	bit b[32] = B; 
	bit c[32] = C; 
	bit d[32] = D;
	a = FF(a, b, c, d, M[0], 7, 0xd76aa478);
	d = FF(d, a, b, c, M[1], 12, 0xe8c7b756);
	c = FF(c, d, a, b, M[2], 17, 0x242070db);
	b = FF(b, c, d, a, M[3], 22, 0xc1bdceee);
	...
	a = II(a, b, c, d, M[4], 6, 0xf7537e82);
	d = II(d, a, b, c, M[11], 10, 0xbd3af235);
	c = II(c, d, a, b, M[2], 15, 0x2ad7d2bb);
	b = II(b, c, d, a, M[9], 21, 0xeb86d391);
	A = sum(A, a, 32);
	B = sum(B, b, 32);
	C = sum(C, c, 32);
	D = sum(D, d, 32);
	
	Out[0] = A; Out[1] = B;	Out[2] = C; Out[3] = D;
}
