__in  bit M[16][32];
__out bit Out[4][32];

// �������������� ���������� ���������
bit A[32] = 0x67452301;
bit B[32] = 0xEFCDAB89;
bit C[32] = 0x98BADCFE;
bit D[32] = 0x10325476;
// ���������� �������
bit F(bit X[32], bit Y[32], bit Z[32])
{
	return (X&Y)|(!X&Z);
}
bit FF(bit a[32], bit b[32], bit c[32], bit d[32], 
       bit M[32], int s, bit t[32])
{
	a = sum(sum(sum(a, F(b, c, d), 32), M, 32), t, 32);
	return b + (a <<< s);
}
...